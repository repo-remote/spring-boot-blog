package com.itstyle.blog.common.constant;

/**
 *  系统级静态变量
 * 	@Author  小柒2012
 *  @Date	 2019年1月21日
 */
public class SystemConstant {

	/**
	 * 分割符-文件分隔符
	 */
	public static final String SF_FILE_SEPARATOR = System.getProperty("file.separator");

	/**
	 * 数据标识
	 */
	public static final String DATA_ROWS = "rows";

	/**
	 * 成功
	 */
	public static final String SUCCESS = "success";
	/**
	 * 失败
	 */
	public static final String ERROR = "error";
	/**
	 * 分頁
	 */
	public static final Integer PAGE_SIZE = 10;

}
